#include "server.h"
#include "utils.h"

int		pasv_func(t_user *user)
{
	user->mode = PASSIVE;
//	send_reply(((t_user *)user)->sock, REP_200, PARAM, 200);
	return (EXIT_SUCCESS);
}

int		quit_func(t_user *user)
{
	send_reply(user->user_sock, REP_221, CTRL);
	return (EXIT_SUCCESS);
}

int		syst_func(t_user *user)
{
	send_reply(user->user_sock, REP_215, CTRL);
	return (EXIT_SUCCESS);
}

int		pwd_func(t_user *user)
{
	char	resp[PATH_MAX];
	char	*rep;
	int		len;

	len = 0;
	ft_strcpy(&resp[len], REP_257);
	len = ft_strlen(REP_257);
	ft_strcpy(&resp[len], user->path);
	len += ft_strlen(user->path);
	ft_strcpy(&resp[len], "\" is the current directory");
	send_reply(user->user_sock, resp, CTRL);
	return (EXIT_SUCCESS);
}

int		user_func(t_user *user)
{
	if (user->cmd->opt)
	{
		if (user->name)
			free(user->name);
		user->name = ft_strdup(user->cmd->opt);
		if (ft_strcmp(user->name, "anonymous") == 0)
			send_reply(user->user_sock, REP_331, CTRL);
		else
			send_reply(user->user_sock, REP_530, FAIL);
	}
	else
		send_reply(user->user_sock, REP_530, FAIL);
	return (EXIT_SUCCESS);
}

int		pass_func(t_user *user)
{
//	send_reply(((t_user *)user)->sock, REP_200);
//	ft_putstr_fd("OK1\n", 1);
	if (user->cmd->opt)
	{
//		ft_putstr_fd("OK2\n", 1);
		if (user->pass)
			free(user->pass);
		user->pass = ft_strdup(user->cmd->opt);
		send_reply(user->user_sock, REP_530, CTRL);
	}
	else if (ft_strcmp(user->name, "anonymous") == 0)
		send_reply(user->user_sock, REP_230, CTRL);
	return (EXIT_SUCCESS);
}
