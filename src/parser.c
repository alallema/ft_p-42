#include "server.h"
#include "utils.h"

static t_cmd_token	g_cmd_token[CMD_NB] =
{
	{ "PORT", &port_func },
	{ "PASV", &pasv_func },
	{ "USER", &user_func },
	{ "PASS", &pass_func },
	{ "SYST", &syst_func },
	{ "CWD", &cwd_func },
	{ "PWD", &pwd_func },
	{ "QUIT", &quit_func },
	{ "LIST", &list_func },
	{ "RETR", &retr_func },
	{ "STOR", &user_func },
	{ "STRU", &user_func },
	{ "MODE", &user_func }
};


int		tokenizer(t_user *user)
{
	int		i;
	i = 0;
	while (i < CMD_NB)
	{
		if (ft_strcmp(user->cmd->cmd, g_cmd_token[i].cmd) == 0)
		{
			g_cmd_token[i].f(user);
			if (i != 0 && user->cmd_sock != 0)
			{
				close_channel(user);
				user->cmd_sock = 0;
			}
			return (EXIT_SUCCESS);
		}
		i++;
	}
	send_reply(user->user_sock, REP_500, CTRL);
	return (EXIT_FAILURE);
}

int		lexer(t_user *user)
{
	if (user->cmd->opt)
	{
		printf(YEL_B"[RECV]"RESET" cmd: %s", user->cmd->cmd);
		printf(" opt: %s\n", user->cmd->opt);
	}
	else
		printf(YEL_B"[RECV]"RESET" cmd: %s\n", user->cmd->cmd);
	tokenizer(user);
	clean_cmd(user);
	return (EXIT_SUCCESS);
}

int		parse_cmd(char *buf, t_user *user)
{
	int		sp;
	int		len;
	char	*opt;
	t_cmd	*cmd;

	sp = 0;
	len = ft_strlen(buf);
	buf[len - 2] = '\0';
	len = len - 2;
	while (buf[sp] && buf[sp] != ' ')
		sp++;
	user->cmd->cmd = ft_memalloc(sp + 1);
	user->cmd->cmd = strncpy(user->cmd->cmd, buf, sp);
	if (user->cmd->opt)
		ft_memdel((void *)&(user->cmd->opt));
	if (sp + 1 < len)
	{
		user->cmd->opt = ft_memalloc(len - sp + 1);
		user->cmd->opt = strncpy(user->cmd->opt, &(buf[sp + 1]), len - sp);
	}
	lexer(user);
	return (EXIT_SUCCESS);
}

void	clean_cmd(t_user *user)
{
	if (user->cmd->cmd)
		ft_memdel((void **)&(user->cmd->cmd));
	if (user->cmd->opt)
		ft_memdel((void **)&(user->cmd->opt));
}
