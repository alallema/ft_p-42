#include "server.h"
#include "utils.h"

int		tablen(char **tab)
{
	int		len;

	len = 0;
	while (tab[len])
		len++;
	return (len);
}

char	*format_addr(char **arg)
{
	char	*addr;
	int		i;
	int		len;

	i = 1;
	len = 0;
	if ((addr = ft_memalloc(sizeof(16))) == NULL)
		return (NULL);
	ft_strcpy(&addr[len], arg[0]);
	while (i < 4)
	{
		len = ft_strlen(addr);
		ft_strcpy(&addr[len], ".");
		ft_strcpy(&addr[len + 1], arg[i]);
		i++;
	}
	return (addr);
}

uint16_t		format_port(char **arg)
{
	uint16_t	port;
	uint8_t		port_high;
	uint8_t		port_low;

	port_high = (uint8_t)ft_atoi(arg[0]);
	port_low = (uint8_t)ft_atoi(arg[1]);
	port = 0;
	port = (uint16_t)(port_high << 8 | port_low);
	return (port);
}

int		port_func(t_user *user)
{
	char	*addr;
	char	**tab;
	int		port;
	int		sock;

	tab = ft_strsplit(user->cmd->opt, ',');
	if (tablen(tab) != 6)
		return (error(ERR_ADDR));
	addr = NULL;
	port = 0;
	user->mode = ACTIVE;
	if ((addr = format_addr(tab)) == NULL)
		return (error(ERR_MALL));
	port = format_port(&tab[4]);
	clean_tab(tab);
	if (open_channel(user, addr, port) == EXIT_SUCCESS)
		send_reply(user->user_sock, REP_200, PARAM);
	else
		send_reply(user->user_sock, REP_425, FAIL);
	ft_memdel((void **)&addr);
	return (EXIT_SUCCESS);
}
