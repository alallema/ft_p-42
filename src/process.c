#include "server.h"
#include "utils.h"

int		create_socket(int sock)
{
	int						cs;
	unsigned int			cslen;
	struct sockaddr_in		csin;

	if ((cs = accept(sock, (struct sockaddr *)&csin, &cslen)) < 0)
		return (error(ERR_ACCEPT));
	return (cs);
}

int		handle_session(int sock)
{
	pid_t	pid;
	int		status;
	int		options;
	int		cs;

	options = 0;
	while (1)
	{
		cs = create_socket(sock);
		pid = fork();
		signal(SIGCHLD, sigchld_handler);
		if (pid < 0)
		{
			error(ERR_FORK);
			send_reply(cs, REP_421, FAIL);
		}
		if (pid == 0)
		{
			close(g_server_sock);
			server_run(cs);
		}
		close(cs);
	}
	return (EXIT_SUCCESS);
}
//pid_t wait4(pid_t pid, int *stat_loc, int options, struct rusage *rusage);

//WIFEXITED(status)
//	True if the process terminated normally by a call to _exit(2) or exit(3).

//WEXITSTATUS(status)
//	If WIFEXITED(status) is true, evaluates to the low-order 8 bits of the argument passed to _exit(2) or
//	exit(3) by the child.
//	This macro should only be employed if WIFEXITED returned true.

//WIFSIGNALED(status)
//	True if the process terminated due to receipt of a signal.

//WSTOPSIG(status)
//	If WIFSTOPPED(status) is true, evaluates to the number of the signal that caused the process to stop.

//int	execv(const char *path, char *const argv[]);
//		char	**s;
//		s = malloc(sizeof(void *) * 2);
//		s[0] = ft_strdup("/bin/pwd");
//		s[1] = NULL;
//		execve(s[0], s, envp);
