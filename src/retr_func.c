#include "server.h"
#include "utils.h"

t_file		get_file_fd(char *filename)
{
	int				fd;
	struct stat		st;
	t_file			file;

	file.fd = 0;
	file.size = 0;
	if ((fd = open(filename, O_RDONLY)) < 0)
		return file;
	if (fstat(fd, &st) < 0)
		return file;
	if (S_ISREG(st.st_mode))
	{
		file.fd = fd;
		file.size = st.st_size;
	}
	return file;
}

int		retr_func(t_user *user)
{
	int			fd;
	t_file		file;
	const void	*ptr;

	file = get_file_fd(user->cmd->opt);
//	printf("fd: %d, size: %lld", file.fd, file.size);
	if (file.fd <= 0)
	{
		send_reply(user->user_sock, REP_451, FAIL);
		return (EXIT_FAILURE);
	}
	send_reply(user->user_sock, REP_150, SERV);
	if ((ptr = mmap(NULL, file.size, PROT_READ, MAP_PRIVATE, file.fd, 0)) == NULL)
	{
		send_reply(user->user_sock, REP_451, FAIL);
		close(file.fd);
		return (EXIT_FAILURE);
	}
	else
	{
		if (send_file(user->cmd_sock, ptr, file.size, 0) == EXIT_FAILURE)

			send_reply(user->user_sock, REP_451, FAIL);
		else
			send_reply(user->user_sock, REP_226, SERV);
	}
	close(file.fd);
	return (EXIT_SUCCESS);
}

//int fstat(int fd, struct stat *buf);
//#define        S_IFREG  0100000  /* regular */
//mode_t   st_mode;
