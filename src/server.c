#include "string.h"
#include "server.h"
#include "utils.h"
#include <unistd.h>

int		create_server(int port)
{
	int					sock;
	struct protoent		*proto;
	struct sockaddr_in	sin;

	proto = getprotobyname("tcp");
	if (proto == 0)
		return (EXIT_FAILURE);
	sock = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sock, (const struct sockaddr *)&sin, sizeof(sin)) == -1)
		return(error(ERR_BIND));
	listen(sock, BACKLOG);
	printf(PRR"FTP-Server: Ftp server launch on port %d"RESET"\n", port);
	g_server_sock = sock;
	return (sock);
}

int		server_listen(char *buf, int client_sock, int rep, t_user *user)
{
	buf[rep] = '\0';
	print_recv(buf, rep);
	if (ft_strcmp(buf, "quit") == 0 || ft_strcmp(buf, "QUIT") == 0)
		return(EXIT_SUCCESS);
	parse_cmd(buf, user);
	return (1);
}

int		server_run(int cs)
{
	int				rep;
	char			buf[1024];
	t_user			*user;

	ft_putstr("FTP-server: Begin connexion to client\n");
	if (!(g_root_path = getcwd(g_root_path, PATH_MAX)))
		error(ERR_CWD);
	user = user_conn(cs);
	while ((rep = recv(cs, &buf, 1024, 0)) > 0)
	{
		if (server_listen(buf, cs, rep, user) <= 0)
			break;
	}
	printf(PRR"FTP-Server:User quit ftp client"RESET"\n");
	destroy_user(user);
	close(cs);
	exit (EXIT_SUCCESS);
}

int		main(int ac, char **av)
{
	uint16_t	port;
	int			sock;

	if (ac != 2)
		usage(av[0], SERVER);
	port = ft_atoi(av[1]);
	signal(SIGINT, sigint_handler);
	if ((port = check_port(port)) == EXIT_FAILURE)
		exit (EXIT_FAILURE);
	if ((sock = create_server(port)) != EXIT_FAILURE)
		handle_session(sock);
//		server_run(sock);
	close(g_server_sock);
	return (0);
}
