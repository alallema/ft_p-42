#include "server.h"
#include "utils.h"

void	sigint_handler(int sig)
{
	if (sig == SIGINT)
	{
		printf(PRR"FTP-Server: User process excited\nGoobye\n"RESET);
		ft_memdel((void **)&g_root_path);
		close(g_server_sock);
	}
	exit (EXIT_SUCCESS);
}

void	sigchld_handler(int sig)
{
	int		status;

	wait4(-1, &status, 0, NULL);
	printf("FTP-Server: client process exited\n");
}
