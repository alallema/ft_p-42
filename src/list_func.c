#include "server.h"
#include "utils.h"

char	*get_ls_path(t_user *user)
{
	char	*new_user_path;
	char	*new_path;
	char	*path;

	path = user->cmd->opt;
	if (!path || (path && path[0] == '-'))
		return (NULL);
	if (path && ft_strcmp(path, "/") == 0)
		return (g_root_path);
	else
	{
		if (handle_path(user, &new_path, &new_user_path) == FALSE)
			return (NULL);
		else
			user->cmd->opt = new_user_path;
		ft_memdel((void **)&new_path);
		ft_memdel((void **)&new_user_path);
	}
	return (path);
}

void	exec_ls(t_user *user)
{
	char	*arg[4];

	arg[0] = "/bin/ls";
	arg[1] = "-l";
	arg[2] = get_ls_path(user);
	arg[3] = NULL;
	send_reply(user->user_sock, REP_150, SERV);
	if (dup2(user->cmd_sock, STDOUT_FILENO) < 0
		|| dup2(user->cmd_sock, STDERR_FILENO) < 0)
	{
		send_reply(user->user_sock, REP_426, FAIL);
		exit(EXIT_FAILURE);
	}
	execv(arg[0], arg);
}

int		handle_exec(t_user *user)
{
	pid_t	pid;
	int		status;
	int		options;

	pid = fork();
	options = 0;
	if (pid < 0)
	{
		send_reply(user->user_sock, REP_451, FAIL);
		return(error(ERR_FORK));
	}
	if (pid == 0)
		exec_ls(user);
	if (pid > 0)
	{
		wait4(pid, &status, options, NULL);
		if (WIFSIGNALED(status)){
//			printf("Kill by signal %d\n", WTERMSIG(status));
//			printf("Parent: Child exited with status: %d\n", WEXITSTATUS(status));
			send_reply(user->user_sock, REP_226, SERV);
		}
		else if (!WIFEXITED(status))
		{
			send_reply(user->user_sock, REP_451, FAIL);
			return (EXIT_FAILURE);
		}
		if (WIFEXITED(status))
		{
//			printf("Parent: Child exited with status: %d\n", WEXITSTATUS(status));
			send_reply(user->user_sock, REP_226, SERV);
		}
	}
	return (EXIT_SUCCESS);
}

int		list_func(t_user *user)
{
	handle_exec(user);
	return (EXIT_SUCCESS);
}
