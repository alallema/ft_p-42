#include "server.h"
#include "utils.h"

int		destroy_cmd(t_cmd *cmd)
{
	if (cmd->cmd)
		ft_memdel((void **)&(cmd->cmd));
	if (cmd->opt)
		ft_memdel((void **)&(cmd->opt));
	if (cmd)
		ft_memdel((void **)&(cmd));
	return (EXIT_SUCCESS);
}

t_cmd	*init_cmd_struct(void)
{
	t_cmd	*cmd;

	cmd = ft_memalloc(sizeof(t_cmd));
	if (cmd == NULL)
		return (NULL);
	cmd->cmd = NULL;
	cmd->opt = NULL;
	return (cmd);
}

int		destroy_user(t_user *user)
{
	if (user->name)
		ft_memdel((void **)&(user->name));
	if (user->pass)
		ft_memdel((void **)&(user->pass));
	if (user->path)
		ft_memdel((void **)&(user->path));
	if (user->cmd)
	{
		destroy_cmd(user->cmd);
		user->cmd = NULL;
	}
	if (user)
		ft_memdel((void **)&(user));
	return (EXIT_SUCCESS);
}

t_user	*init_user(void)
{
	t_user		*user;

	user = ft_memalloc(sizeof(t_user));
	if (user == NULL)
		return (NULL);
	user->name = NULL;
	user->pass = NULL;
	user->path = ft_strdup("/");
	user->user_sock = 0;
	user->cmd_sock = 0;
	user->mode = 0;
	return (user);
}

t_user		*user_conn(int client_sock)
{
	t_user		*user;

	if ((user = init_user()) == NULL)
	{
		error(ERR_MALL);
		return (NULL);
	}
	user->user_sock = client_sock;
	user->cmd = init_cmd_struct();
	send_reply(client_sock, REP_220, CTRL);
	return (user);
}
