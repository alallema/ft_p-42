#include "server.h"
#include "utils.h"

int		print_recv(char *buf, int rep)
{
	if (buf && rep)
	{
		printf("receive: %d bytes\n", rep);
		return (EXIT_SUCCESS);
	}
	return (EXIT_FAILURE);
}

int		print_server_act(char *type, char *resp, char *way)
{
	if (type && way && resp)
	{
		if (ft_strcmp(type, "[FAIL] ") == 0)
			printf(ORA"%s"RESET, type);
		else
			printf(GRE_B"%s"RESET, type);
		printf("%.*s%s\n", 3, resp, way);
		return (EXIT_SUCCESS);
	}
	return (EXIT_FAILURE);
}
