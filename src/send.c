#include "server.h"
#include "utils.h"

int		send_reply(int cs, char *response, char *rep)
{
	char	*resp;

	resp = ft_strjoin(response, "\r\n");
	if ((send(cs, resp, ft_strlen(resp), 0)) < 0)
		return (error(ERR_SEND));
	print_server_act(rep, resp, SEND);
	return (EXIT_SUCCESS);
}

int		send_file(int sock, const void *buffer, size_t length, int flags)
{
    ssize_t ret;
    const char *p = buffer;
    while (length > 0)
    {
        ret = send(sock, p, length, flags);
        if (ret < 0)
            return (EXIT_FAILURE);
        p += ret;
        length -= ret;
    }
    return (EXIT_SUCCESS);
}
//int		send_mult_reply(int cs, char *rep, char *s)
//{
//	char	*resp;
//
//	resp = ft_strjoin(rep, s);
//	if ((send(cs, resp, ft_strlen(resp), 0)) < 0)
//		return (error(ERR_SEND));
//	return (EXIT_SUCCESS);
//}
