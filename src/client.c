#include "client.h"
#include "utils.h"
#include <stdio.h>

int		client_run(int port, int sock)
{
	int		ret;
	int		bytes;
	char	*buf;

	buf = NULL;
	ret = 0;
	while (TRUE)
	{
		ft_putstr("OK1\n");
//		while ((ret = recv(sock, buf,  4096 - 1, 0)) > 0)
		while ((ret = get_next_line(0, &buf)) > 0)
		{
			printf("cmd: %s\nreturn: %d\n", buf, ret);
//			ret = recv(sock, buf,  4096 - 1, 0);
			if ((bytes = send(sock, buf, ft_strlen(buf), 0)) == -1 )
			{
				ft_putstr_fd("Send error\n", 2);
				return(EXIT_FAILURE);
			}
			else
			{
				ft_putstr("receive ");
				ft_putnbr(bytes);
				ft_putstr(" bytes\n");
			}
			ret = 0;
			bytes = 0;
		}
	}
	return (EXIT_SUCCESS);
}

int		main(int ac, char **av)
{
	int		port;
	int		sock;
	int		ret;

	if (ac != 3)
		usage(av[0], CLIENT);
	port = ft_atoi(av[2]);
	if ((sock = create_client(av[1], (uint16_t)port)) == EXIT_FAILURE)
		exit(EXIT_FAILURE);
	if ((client_run(port, sock)) == EXIT_FAILURE)
		exit(EXIT_FAILURE);
	close(sock);
	return (EXIT_SUCCESS);
}
