#include "server.h"
#include "utils.h"

int		open_channel(t_user *user, char *addr, int port)
{
	int		sock;

	if ((check_port(port) != EXIT_FAILURE) && ((sock = create_client(addr, port)) != EXIT_FAILURE))
	{
		user->cmd_sock = sock;
		return (EXIT_SUCCESS);
	}
	return (EXIT_FAILURE);
}

int		close_channel(t_user *user)
{
	if (user->cmd_sock > 0)
	{
		if (close(user->cmd_sock) > 0)
		{
			user->cmd_sock = 0;
			return (2);
		}
	}
	return (EXIT_FAILURE);
}
