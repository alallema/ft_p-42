# include "utils.h"

void	usage(char *str, int who)
{
	printf("Usage: %s", str);
	if (who == CLIENT)
		ft_putstr(" <addr> <port>\n");
	if (who == SERVER)
		ft_putstr(" <port>\n");
	exit (EXIT_FAILURE);
}

int		create_client(char *addr, uint16_t port)
{
	int					sock;
	struct protoent		*proto;
	struct sockaddr_in	sin;

//	printf("addr: %s port: %d\n", addr, port);
	proto = getprotobyname("tcp");
	if (proto == 0)
		return (-1);
	if ((sock = socket(PF_INET, SOCK_STREAM, proto->p_proto)) < 0)
		return(error(ERR_SOCK));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	if ((sin.sin_addr.s_addr = inet_addr(addr)) == INADDR_NONE)
		return (error(ERR_ADDR));
	if (connect(sock, (const struct sockaddr *)&sin, sizeof(sin)) < 0)
		return(error(ERR_CONN));
	return (sock);
}

int		error(char *err)
{
	printf("FTP-Error: %s\n", err);
	return (EXIT_FAILURE);
}

int		print_error(char *s, char *err)
{
	printf("%s ", s);
	error(err);
	return (EXIT_FAILURE);
}

int		check_port(uint16_t port)
{
	if (port < IPPORT_RESERVED || port > IPPORT_HILASTAUTO)
		return(error(ERR_PORT));
	return (port);
}

void	clean_tab(char **tab)
{
	while (*tab)
	{
		ft_memdel((void **)tab);
		tab++;
	}
	ft_memdel((void **)tab);
}
