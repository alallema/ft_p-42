#include "server.h"
#include "utils.h"

int		is_valid_dir(const char *dirname)
{
	DIR				*dir;
	struct stat		st;
	int				fd;

	dir = NULL;
	ft_memset(&st, 0, sizeof(struct stat));
	if ((fd = open(dirname, O_RDONLY)) < 0)
		return (EXIT_FAILURE);
	fstat(fd, &st);
	if (S_ISDIR(st.st_mode) || S_ISLNK(st.st_mode))
	{
		if ((dir = opendir(dirname)))
		{
			closedir(dir);
			return (EXIT_SUCCESS);
		}
		return (print_error((char *)dirname, ERR_NO_ACCESS));
	}
	else
		return (print_error((char *)dirname, ERR_NO_DIR));
}

void	print_tab(char **path_tab)
{
	int		i;

	i = 0;
	if (path_tab == NULL)
	{
		printf("tab :NULL\n");
		return;
	}
	while (path_tab[i])
	{
		printf("tab %d: %s\n", i, path_tab[i]);
		i++;
	}
}

char	*create_path_from_tab(char **path_tab)
{
	int		i;
	int		j;
	int		len;
	char	*new_path;

	i = 0;
	j = 0;
	len = 0;
	if (path_tab == NULL)
		return (NULL);
	while (path_tab[len])
	{
		while (path_tab[len][i])
			i++;
		j = j + i;
		len++;
	}
	new_path = ft_memalloc(sizeof(j));
	i = 0;
	len = 0;
	while (path_tab[len])
	{
		new_path = ft_strjoin(new_path, "/");
		new_path = ft_strjoin(new_path, path_tab[len]);
		len++;
	}
	clean_tab(path_tab);
	return (new_path);
}

char	**cut_tab(char **path_tab, int i)
{
	char	**tab;
	int		len;
	int		len2;
	int		j;

	j = 0;
	len = 0;
	len2 = 0;
	while (path_tab[len])
		len++;
	if (len - j == 0)
		return (NULL);
	if (!(tab = (char **)malloc(sizeof(char *) * (len + 1 - i))))
		return (NULL);
	tab[len - i] = NULL;
	i = 0;
	while (i < len)
	{
		if (ft_strcmp(path_tab[i], "") != 0)
		{
			tab[j] = ft_strdup(path_tab[i]);
			j++;
		}
		i++;
	}
	clean_tab(path_tab);
	return (tab);
}

char	*get_true_path(char *path)
{
	char	**path_tab;
	int		i;

	i = 0;
	path_tab = ft_strsplit(path, '/');
	while (path_tab && path_tab[i])
	{
		if ((ft_strcmp(path_tab[i], "..") == 0 && i == 0)
		|| ft_strcmp(path_tab[i], ".") == 0)
		{
			path_tab[i] = ft_strdup("");
			path_tab = cut_tab(path_tab, 1);
			i = -1;
		}
		else if (ft_strcmp(path_tab[i], "..") == 0)
		{
			path_tab[i - 1] = ft_strdup("");
			path_tab[i] = ft_strdup("");
			path_tab = cut_tab(path_tab, 2);
			i = -1;
		}
		i++;
	}
	path = create_path_from_tab(path_tab);
	return (path);
}

char	*concat_path(char *current_dir, char *new)
{
	char	*new_path;
	char	*tmp;
	int		len;

	len = ft_strlen(current_dir) + ft_strlen(new);
	new_path = ft_memalloc(sizeof(len));
	tmp = ft_memalloc(sizeof(len));
	if (current_dir[ft_strlen(current_dir) - 1] != '/')
	{
		tmp = ft_strjoin(current_dir, "/");
		new_path = ft_strjoin(tmp, new);
	}
	else
		new_path = ft_strjoin(current_dir, new);
	ft_memdel((void **)&tmp);
	return (new_path);
}

int		handle_path(t_user *user, char **new_path, char **new_user_path)
{
	if (ft_strcmp(user->cmd->opt, "/") == 0 && ft_strcmp(user->path, "/") == 0)
		return (FALSE);
	if (ft_strcmp(user->cmd->opt, "/") == 0 && ft_strcmp(user->path, "/") != 0)
	{
		*new_user_path = ft_strdup("/");
		*new_path = ft_strdup(g_root_path);
		return (TRUE);
	}
	if ((user->cmd->opt)[0] == '/')
		*new_user_path = ft_strdup(user->cmd->opt);
	else
		*new_user_path = concat_path(user->path, user->cmd->opt);
	if ((*new_user_path = get_true_path(*new_user_path)))
	{
		if (ft_strcmp(*new_user_path, "") == 0)
		{
			*new_user_path = ft_strdup("/");
			*new_path = ft_strdup(g_root_path);
		}
		else
			*new_path = concat_path(g_root_path, *new_user_path);
		return (TRUE);
	}
	return (FALSE);
}

int		cwd_func(t_user *user)
{
	char	*new_user_path;
	char	*new_path;
	int		ret;

	new_path = NULL;
	new_user_path = NULL;
	ret = handle_path(user, &new_path, &new_user_path);
	if (ret == FALSE)
		send_reply(user->user_sock, REP_250, CTRL);
	else if (ret == TRUE && (is_valid_dir(new_path) == EXIT_SUCCESS) && chdir(new_path) == 0)
	{
		user->path = ft_strdup(new_user_path);
		send_reply(user->user_sock, REP_250, CTRL);
	}
	else
		send_reply(user->user_sock, REP_550, FAIL);
	ft_memdel((void **)&new_path);
	ft_memdel((void **)&new_user_path);
	return (EXIT_SUCCESS);
}
