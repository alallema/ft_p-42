RED =			\033[31m
GRE =			\033[32m
YEL =			\033[33m
BLU =			\033[34m
PUR =			\033[94m
PIN =			\033[1;35m
PRR =			\033[0;36m
STD =			\033[39m

NAME_CLIENT =	client
NAME_SERVER =	server

IDIR =			./inc/
ILIB =			./libft/inc
INCS =			client.h		\
				server.h		\
				utils.h

INCC =			$(addprefix $(IDIR),$(INCS))

LDIR =			./libft
LIBS =			-lft

VPATH =			./src/:

SRC_CLIENT =	client.c		\
				utils.c

SRC_SERVER =	server.c		\
				utils.c			\
				user.c			\
				parser.c		\
				cmd_ctrl.c		\
				send.c			\
				cwd_func.c		\
				port_func.c		\
				list_func.c		\
				data_channel.c	\
				signal.c		\
				process.c		\
				retr_func.c		\
				print.c

SRCC =			$(addprefix $(SRC_CLIENT),$(SRC_SERVER))

ODIR =			./objs/

OBJS_CLIENT =	$(SRC_CLIENT:.c=.o)
OBCC_CLIENT =	$(addprefix $(ODIR),$(OBJS_CLIENT))

OBJS_SERVER =	$(SRC_SERVER:.c=.o)
OBCC_SERVER =	$(addprefix $(ODIR),$(OBJS_SERVER))

NORM =			$(SRCC) $(INCC)

CFLAG =			-Wall -Wextra -Werror -I$(IDIR)

all: $(NAME_CLIENT) $(NAME_SERVER)

client: $(NAME_CLIENT)

server: $(NAME_SERVER)

$(NAME_CLIENT): header $(OBCC_CLIENT)
	@echo "  ${PUR}++ Compilation ++ :${STD} $@"
	@make -C ./libft/
	@gcc -g $(CFLAG) $(OBCC_CLIENT) -L$(LDIR) $(LIBS) -o $(NAME_CLIENT)

$(NAME_SERVER): header $(OBCC_SERVER)
	@echo "  ${PUR}++ Compilation ++ :${STD} $@"
	@make -C ./libft/
	@gcc -g $(FLAG) $(OBCC_SERVER) -L$(LDIR) $(LIBS) -o $(NAME_SERVER)
	@echo "  ${PIN}Compilation terminee !${STD}"

$(ODIR)%.o: $(SRCC_PATH)%.c
	@echo "  ${PUR}+Compilation :${STD} $^"
	@mkdir -p $(ODIR)
	@gcc $^ $(FLAG) -c -o $@ -I$(IDIR) -I$(ILIB)

#	@echo "${PRR}"
#	@echo "   ========================\n"
#	@echo "  ___________ __"
#	@echo "  \\_   _____//  |_   ______"
#	@echo "  |    __) \\   __\\  \\____ \\"
#	@echo "  |     \   |  |    |  |_> >"
#	@echo "  \___  /   |__|____|   __/"
#	@echo "      \/      /_____/__|\n"
#	@echo "   ========================"
#	@echo "${STD}"

header:
	@echo "${PRR}"
	@echo "  ____    __"
	@echo " /\  _\`\ /\ \__"
	@echo " \ \ \ \_\ \ ,_\          _____"
	@echo "  \ \  _\/\ \ \/         /\ ’__\`\ "
	@echo "   \ \ \/  \ \ \_        \ \ \ \ \ "
	@echo "    \ \_\   \ \__\        \ \ ,__/ "
	@echo "     \/_/    \/__/  _______\ \ \/ "
	@echo "                   /\_______\\ \_\ "
	@echo "                   \/______/ \/_/ "
	@echo "${STD}"

norme: header
	@echo "${PRR}  Verification de la norme${STD}\n"
	@norminette $(NORM)
	@echo "${RED}  \nTotal errors :${STD}" $(shell norminette $(NORM) | grep -v "Norme" | wc -l)

clean: header
	@echo "  ${RED}-Delete all object files${STD}"
	@rm -rf $(ODIR)
	@make -C ./libft/ fclean
	@rm -f $(OBCC)

fclean: clean
	@rm -f $(NAME_CLIENT) $(NAME_SERVER)
	@make -C ./libft/ fclean
	@echo "  ${RED}-Delete objects and binary${STD}"

re: fclean all
