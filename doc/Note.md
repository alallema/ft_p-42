# Notes

socket -> IPV4 PF_INET Protocole Family Inet
protocole:
- SOCK_STREAM ouvre un canal de communication /TCP en flux
- SCOK_DGRAM communication coup par coup (paquet)

getprotobyname -> recupere numero de protocole qui correspond au protocole envoyer<br>
getprobyname - getprotoent<br>
bind -> associe un nom a une socket address<br>
AF_INET -> Address Family Internet<br>
htons()--"Host to Network Short"<br>
htonl()--"Host to Network Long"<br>
ntohs()--"Network to Host Short"<br>
ntohl()--"Network to Host Long"<br>
listen -> size of queue<br>
/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/netinet/in.h

## Bonus a envisager:
- PASV - mode passif
- Mode flux, block ou compresse
- droit USER ? PASS

## From doc RFC

En général, il est de la responsabilité des serveurs de maintenir le canal de données
actif—de l'initialiser et de le clore. L'exception à cette règle est lorsque le USER-DTP
envoie des données dans un mode qui implique que la fin de fichier (EOF) correspond
à la fermeture de la transmission. Le serveur DOIT fermer le canal de données sous les
conditions suivantes :
1. Le serveur à terminé la transmission de données dans un mode ou la fin de fichier
est signalée par une fermeture du canal.
 2. Le serveur reçoit une commande ABORT de l'utilisateur.
 3. La spécification du port "données" est changée par une commande de l'utilisateur.
 4. Le canal de contrôle est fermé par une procédure normale ou pour toute autre raison.
RFC959 18
 5. Une condition d'erreur irrécupérable est apparue.<br>
Dans tous les autres cas la fermeture est une prérogative du serveur, l'exercice de la
quelle doit être signalée au processus utilisateur par un code de réponse 250 ou 226
seulement.

Seul un USER-PI peut initialiser un canal sur un port autre que standard

PORT - spécifie un port de données non standard à "viser" par le serveur<br>
PASV - Le USER-PI peut demander au serveur de s'identifier au serveur "cible" exprimé par ce port non
standard via la commande PASV.<br>
Le mode de transfert en "flux" est par nature non fiable (ferme le canal à la fin du transfert de fichier)<br>
Tous les transferts de données doivent s'achever par la transmission d'une séquence de fin-de-fichier (EOF), la quelle peut être explicite, ou implicitement déduite RFC959 19 de la fermeture du canal.<br
>

- MODE flux
- MODE bloc<br>
L'en-tête contient un champ de comptage de blocs, et un code de description - 3 octets.<br>
16 bits de poids faible représentent le compte d'octets, 8 bits de poids fort donnent le code.
- MODE compresse<br>
Probablement utilisation du mode block avec entete, option mode flux ?

Erreur et reprise de transmission code 110 immédiatement suivi RETR, STOR ou LIST

ABOR, STAT, QUIT

Les serveurs doivent accepter une nouvelle 
commande USER à tout moment en vue de changer les droits et privilèges
d'accès, ou le compte.

## Todo

FIX cd ../../.. after cd libft cd src -> free
Fix makefile
Rename all function clearly

### Server
>implement signal ✅<br>
>implement fork ✅<br>
>implement parser ✅<br>

- USER NAME (USER) ✅
- PASSWORD (PASS) 
- CHANGE WORKING DIRECTORY (CWD) ✅
- CHANGE TO PARENT DIRECTORY (CDUP) ??
- LOGOUT (QUIT) ✅
- DATA PORT (PORT) ✅
- PASSIVE (PASV)
- LIST (LIST) ✅
- FILE STRUCTURE (STRU)<br>
F - structure-fichier (pas de structure sous-
jacente)<br>
R - structure-enregistrement<br>
P - structure-pages
- TRANSFER MODE (MODE)<br>
S - flux (stream)<br>
B - Bloc<br>
C - Compressé
- RETRIEVE (RETR)
- STORE (STOR)
- SYSTEME (SYST) ? Bonus ?

- send mutiple line response
- send response with size retr and push