#ifndef UTILS_H
# define UTILS_H

# include <netdb.h>
# include <unistd.h>
# include <stdio.h>
# include <sys/syslimits.h>
# include <arpa/inet.h>
# include <signal.h>
# include "libft.h"

#define ERR_BIND "Bind error"
#define ERR_SOCK "Socket error"
#define ERR_CONN "Connect error"
#define ERR_SEND "Send error"
#define ERR_ACCEPT "Accept error"
#define ERR_ADDR "Malformed address request"
#define ERR_MALL "Memory error"
#define ERR_PORT "Port unavailable should be [1024-65335]"
#define ERR_NO_ACCESS " No acces to this directory"
#define ERR_NO_DIR " Directory doesn't exist"
#define ERR_FORK " Error fork"
#define ERR_CWD " Environment Error"

#define CLIENT 0
#define SERVER 1

#define CMD_NB 13

#define RED "\033[31m"
#define GRE "\033[32m"
#define YEL "\033[33m"
#define BLU "\033[34m"
#define PUR "\033[94m"
#define PIN "\033[1;35m"
#define YEL_B "\033[1;33m"
#define GRE_B "\033[1;32m"
#define ORA "\033[1;31m"
#define TUR "\033[0;36m"
#define PRR "\033[1;36m"
#define STD "\033[39m"
#define RESET "\033[0m"
//*#define FOREACH_CMD(str)\
//*		str(PORT)		\
//*		str(PASV)		\
//*		str(USER)		\
//*		str(CWD)		\
//*		str(PWD)		\
//*		str(QUIT)		\
//*		str(LIST)		\
//*		str(RETR)		\
//*		str(STOR)		\
//*		str(STRU)		\
//*		str(MODE)
//*
//*#define GENERATE_STRING(STRING) #STRING,
//*
//*static const char	*g_cmd_array[] = {
//*    FOREACH_CMD(GENERATE_STRING)
//*};

typedef enum	e_bool
{
	FALSE,
	TRUE
}				t_bool;

typedef enum	e_mode
{
	ACTIVE,
	PASSIVE
}				t_mode;

typedef enum	e_cmd_mod
{
	PORT,
	PASV
}				t_cmd_mod;

typedef enum	e_cmd_ctrl
{
	USER,
	CWD,
	PWD,
	QUIT
}				t_cmd_ctrl;

typedef enum	e_cmd_tr
{
	LIST,
	RETR,
	STOR,
	STRU,
	MODE
}				t_cmd_tr;

void	usage(char *str, int who);
int		check_port(uint16_t port);
int		error(char *err);
int		print_error(char *s, char *err);
int		print_recv(char *buf, int rep);
int		create_client(char *addr, uint16_t port);
void	clean_tab(char **tab);

#endif
