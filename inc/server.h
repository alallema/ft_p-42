#ifndef SERVER_H
# define SERVER_H

#include <sys/socket.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>

#define BACKLOG 10

#define REP_150 "150 Here comes the directory listing."
//150 Opening BINARY mode data connection for 100KB.zip (102400 bytes).
//150 Ok to send data.
#define REP_200 "200 EPRT command successful."//Consider using EPSV.
#define REP_215 "215 Unix System Type."//Consider using EPSV.
#define REP_220 "220 Welcome on ftp_p server ! Server ready for a new user."	//connect new user
#define REP_221 "221 Goodbye."	//disconnect
#define REP_226 "226 Directory send OK."
//226 Transfer complete.
//102400 bytes received in 0.0541 seconds (1.8 Mbytes/s)
#define REP_230 "230 Login successful."
//#define REP_530 "530 Login Failed."
#define REP_530 "530 This FTP server is anonymous only."
#define REP_250 "250 Directory successfully changed."
#define REP_257_ "257-"
#define REP_257 "257 \""
#define REP_331 "331 Please specify the password."
#define REP_421 "421 Service not available. Closing connexion."
#define REP_425 "425 Failed to open data connexion"
#define REP_426 "426 Connexion closed. Processing interrupt"
#define REP_451 "451 Connexion closed. Processing Error"
#define REP_500 "500 Command not found."
#define REP_550 "550 Failed to open file."
#define REP_553 "553 Could not create file."

#define CTRL "[CTRL] "
#define SERV "[SERV] "
#define PARAM "[PARAM] "
#define FAIL "[FAIL] "
#define SEND " Send to the client"
#define RECV " Receive from the client"

typedef struct	s_cmd
{
	char	*cmd;
	char	*opt;
}				t_cmd;

typedef struct	s_user
{
	char		*name;
	char		*pass;
	int			user_sock;
	int			cmd_sock;
	int			mode;
	char		*path;
	t_cmd		*cmd;
}				t_user;

typedef struct	s_cmd_token
{
	char	*cmd;
	int		(*f)(t_user *user);
}				t_cmd_token;

typedef struct	s_file
{
	int		fd;
	off_t	size;
}				t_file;

char	*g_root_path;
int		g_server_sock;

int		send_reply(int cs, char *rep, char *resp);
//int		send_mult_reply(int client_sock, char *rep, char *s);
t_user	*user_conn(int client_sock);
int		server_run(int cs);
int		destroy_user(t_user *user);
int		print_server_act(char *type, char *resp, char *way);
int		parse_cmd(char *buf, t_user *user);
int		is_valid_dir(const char *dirname);
int		user_func(t_user *user);
int		pasv_func(t_user *user);
int		pass_func(t_user *user);
int		port_func(t_user *user);
int		cwd_func(t_user *user);
int		list_func(t_user *user);
int		pwd_func(t_user *user);
int		quit_func(t_user *user);
int		syst_func(t_user *user);
void	clean_cmd(t_user *user);
int		open_channel(t_user *user, char *addr, int port);
int		close_channel(t_user *user);
int		handle_path(t_user *user, char **new_path, char **new_user_path);
void	sigint_handler(int sig);
void	sigchld_handler(int sig);
int		handle_session(int sock);
int		retr_func(t_user *user);
int		send_file(int sock, const void *buffer, size_t length, int flags);


#endif

/*
*
*	connect:
*	220 (vsFTPd 3.0.3)
*
*	ls:			[PORT 127,0,0,1,249,161\n]
				[LIST\n]
*	200 EPRT command successful. Consider using EPSV.
*	150 Here comes the directory listing.
*	list_of_directory
*	226 Directory send OK.
*
*	cd:			[CWD /\n]
*	cd directory_name
*
*	250 Directory successfully changed.
*
*	550 Failed to change directory
*
*
*	get:		[PORT 127,0,0,1,249,89\n]
				[RETR file_path\n]
*
*	200 EPRT command successful. Consider using EPSV.
*	550 Failed to open file.
*
*	200 EPRT command successful. Consider using EPSV.
*	550 Failed to open file.
*
*	put:		[PORT 127,0,0,1,249,94\n]
				[STOR file_name]
*
*	200 EPRT command successful. Consider using EPSV.
*	150 Ok to send data.
*	226 Transfer complete.
*	102400 bytes sent in 0.0182 seconds (5.37 Mbytes/s)
*
*	200 EPRT command successful. Consider using EPSV.
*	553 Could not create file.
*
*	pwd:		[PWD\n]
*	257 "/" is the current directory
*
*	quit:		[QUIT\n]
*	221 Goodbye.
*/
