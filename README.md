# ft_p-42

## Description
Ce projet consiste à implémenter un client et un serveur permettant le transfert de fichier en réseau TCP/IP

## Usage

```$> ./server port```

```$> ./client server port```

```
• ls : liste le répertoire courant du serveur
• cd : change le répertoire courant du serveur
• get _file_ : récupère le fichier _file_ du serveur vers le client
• put _file_ : envoi le fichier _file_ du client vers le serveur
• pwd : affiche le chemin du répertoire courant sur le serveur
• quit : coupe la connection + sort du programme
```

## Fonctions autorisees

- socket(2), open(2), close(2), setsockopt(2), getsockname(2)
- getprotobyname(3), gethostbyname(3)
- bind(2), connect(2), listen(2), accept(2)
- htons(3), htonl(3), ntohs(3), ntohl(3)
- inet_addr(3), inet_ntoa(3)
- send(2), recv(2), execv(2), execl(2), dup2(2), wait4(2)
- fork(2), getcwd(3), exit(3), printf(3), signal(3)
- mmap(2), munmap(2), lseek(2), fstat(2)
- opendir(3), readdir(3), closedir(3)
- chdir(2), mkdir(2), unlink(2)
- les fonctions autorisées dans le cadre de votre libft (read(2), write(2), malloc(3),
free(3), etc... par exemple ;-) )
- select(2), FD_CLR, FD_COPY, FD_ISSET, FD_SET, FD_ZERO mais uniquement si c’est pour faire quelque chose de correct !

## Doc
[More on FTP RFC](https://github.com/alallema/ft_p-42/blob/master/doc/File_Transfert_Protocole.pdf)

## Notes

See Doc

## Struct

```
struct sockaddr;

/*
 * Advanced (Full-state) APIs [RFC3678]
 * The RFC specifies uint_t for the 6th argument to [sg]etsourcefilter().
 * We use uint32_t here to be consistent.
 */
	int		setipv4sourcefilter(int, struct in_addr, struct in_addr, uint32_t, uint32_t, struct in_addr *);
	int		getipv4sourcefilter(int, struct in_addr, struct in_addr, uint32_t *, uint32_t *, struct in_addr *);
	int		setsourcefilter(int, uint32_t, struct sockaddr *, socklen_t, uint32_t, uint32_t, struct sockaddr_storage *);
	int		getsourcefilter(int, uint32_t, struct sockaddr *, socklen_t, uint32_t *, uint32_t *, struct sockaddr_storage *);
```


```
struct sockaddr {
	unsigned short    sa_family;    /* famille d'adresse, AF_xxx        */
	char              sa_data[14];  /* 14 octets d'adresse de protocole */
};
```


```
/*
 * Socket address, internet style.
 */
struct sockaddr_in {
	__uint8_t       sin_len;
	sa_family_t     sin_family;		/* Famille d'adresse               */
	in_port_t       sin_port;		/* Numéro de Port                  */
	struct  in_addr sin_addr;		/* Adresse Internet                */
	char            sin_zero[8];	/* Même taille que struct sockaddr */
};
```

```
struct  sockaddr_in {
	short int          sin_family;  /* Famille d'adresse               */
	unsigned short int sin_port;    /* Numéro de Port                  */
	struct in_addr     sin_addr;    /* Adresse Internet                */
	unsigned char      sin_zero[8]; /* Même taille que struct sockaddr */
};
```

## Ports

``` 
 *            ftp://ftp.isi.edu/in-notes/iana/assignments/port-numbers
 *
 *    port numbers are divided into three ranges:
 *
 *                0 -  1023 Well Known Ports
 *             1024 - 49151 Registered Ports
 *            49152 - 65535 Dynamic and/or Private Ports

#define __DARWIN_IPPORT_RESERVED        1024
#define IPPORT_USERRESERVED     5000
#define IPPORT_HIFIRSTAUTO      49152
#define IPPORT_HILASTAUTO       65535```

#define INADDR_ANY              (u_int32_t)0x00000000
#define INADDR_BROADCAST        (u_int32_t)0xffffffff   /* must be masked */
#define INADDR_NONE             0xffffffff              /* -1 return */
```
